'use strict';

angular.module('angulartalkApp')
  .controller('LotsofdataCtrl', function ($scope, $http) {
    $http.get('/api/lotsofdata').success(function(data) {
	 	
      	$scope.data = data;
    });
  });
