'use strict';

angular.module('angulartalkApp')
  .controller('NavbarCtrl', function ($scope, $location) {
    $scope.menu = [{
      'title': 'Home',
      'link': '/'
    },
	{
		'title': 'Lots of data',
		'link' : 'lotsofdata'
	}
	];
    
    $scope.isActive = function(route) {
      return route === $location.path();
    };
  });
