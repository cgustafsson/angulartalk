'use strict';

angular.module('angulartalkApp')
  .filter('startFrom', function () {
    return function (input, start) {
    	return input.slice(start);
    };
  });
